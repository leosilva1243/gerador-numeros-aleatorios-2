import * as _ from 'lodash'

import { fatorial, curva } from '../helpers/matematica'
import { intervalo } from '../helpers/amostra'

export function criarPoisson(
    amostra: number[]
): (number) => number {
    const n = intervalo(amostra).length
    return (x: number): number => {
        const λ = curva(x, amostra)
        return poisson(x, n, λ)
    }
}

export function poisson(x: number, quantidade: number, curva: number) {
    const n = quantidade
    const λ = curva
    return (n * Math.pow(Math.E, -λ) * Math.pow(λ, x)) / fatorial(x)
}
