import { media, desvioPadrao } from '../helpers/matematica';

export function criarNormal(
    amostra: number[]
): (number) => number {
    const µ = media(amostra)
    const ø = desvioPadrao(amostra)
    return (x: number): number => normal(x, µ, ø)
}

export function normal(
    x: number,
    media: number,
    desvioPadrao: number
): number {
    const µ = media
    const ø = desvioPadrao
    return (1 / (ø * Math.sqrt(2 * Math.PI)))
        * Math.exp((-1 / 2) * Math.pow(((x - µ) / ø), 2))
}