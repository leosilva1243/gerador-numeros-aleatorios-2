import {
    menorValor,
    maiorValor,
    valorMaisFrequente
} from '../helpers/amostra'

export function criarTriangular(
    amostra: number[]
): (number) => number {
    const a = menorValor(amostra)
    const b = maiorValor(amostra)
    const c = valorMaisFrequente(amostra)
    return (x: number): number => triangular(x, a, b, c)
}

export function triangular(
    x: number,
    menorValor: number,
    maiorValor: number,
    valorMaisFrequente: number
): number {
    const a = menorValor
    const b = maiorValor
    const c = valorMaisFrequente
    if (x < c) {
        return (2 * (x - a)) / ((b - a) * (c - a))
    }
    if (x > c) {
        return (2 * (b - x)) / ((b - a) * (b - c))
    }
    return 2 * (b - a)
}