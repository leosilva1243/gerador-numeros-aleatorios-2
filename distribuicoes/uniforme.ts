import { intervalo } from '../helpers/amostra'

export function criarUniforme(amostra: number[]): (number) => number {
    const n = intervalo(amostra).length
    return (x: number): number => uniforme(x, n)
}

export function uniforme(x: number, quantidade: number): number {
    const n = quantidade
    return x / n
}