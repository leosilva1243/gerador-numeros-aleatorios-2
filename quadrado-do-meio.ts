import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'

export class QuadradoDoMeio implements GeradorNumerosAleatorios {
    private inicioFatia: number
    private fimFatia: number

    constructor(
        private semente: number
    ) {
        const tamanhoSemente = String(semente).length
        const metadeTamanhoSemente = tamanhoSemente / 2
        this.inicioFatia = metadeTamanhoSemente
        this.fimFatia = metadeTamanhoSemente + tamanhoSemente
    }

    gerarProximo(): number {
        const quadrado = this.semente * this.semente
        const strQuadrado = String(quadrado)
        const meio = strQuadrado.slice(this.inicioFatia, this.fimFatia)
        const valorDoMeio = Number(meio)
        this.semente = valorDoMeio
        return valorDoMeio
    }
}