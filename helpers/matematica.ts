import { memoize } from './memoize'

export const fatorial = memoize((fn, x) => {
    if (x < 2) { return 1; }
    return x * fn(x - 1)
})

export function somatorio(amostra: number[])
export function somatorio(amostra: number[], fn: (x: number, index: number) => number)
export function somatorio(
    amostra: number[], fn?: (x: number, index: number) => number
): number {
    const _fn = fn || ((x, index) => x)
    return amostra.reduce((s, x, i) => s + _fn(x, i), 0)
}

export function media(amostra: number[]): number {
    const soma = amostra.reduce((s, x) => s + x, 0)
    return soma / amostra.length
}

export function desvioPadrao(amostra: number[]): number {
    var µ = media(amostra);

    var difs2 = amostra.map((valor) => {
        var dif = valor - µ;
        var dif2 = dif * dif;
        return dif2;
    });

    var variancia = media(difs2);

    return Math.sqrt(variancia);
}

export function curva(x: number, amostra: number[]): number {
    return somatorio(amostra, (a) => x * a) / somatorio(amostra)
}