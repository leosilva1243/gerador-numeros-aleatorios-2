import * as _ from 'lodash'

export function menorValor(amostra: number[]): number {
    return amostra.reduce((menor, valor) => valor < menor ? valor : menor, amostra[0])
}

export function maiorValor(amostra: number[]): number {
    return amostra.reduce((maior, valor) => valor > maior ? valor : maior, amostra[0])
}

export function valorMaisFrequente(amostra: number[]): number {
    const repeticoes: { [key: number]: number } = {}
    amostra.forEach(valor => {
        repeticoes[valor] = (repeticoes[valor] || 0) + 1
    })
    return amostra.sort((a, b) => a - b).shift()!
}

export function intervalo(amostra: number[]): number[] {
    const menor = menorValor(amostra)
    const maior = maiorValor(amostra)
    return _.range(menor, maior + 1)
}

export function verificarRepeticoes(amostra: number[]): { [key: number]: number } {
    const repeticoes: { [key: number]: number } = {}
    for (const item of amostra) {
        if (repeticoes[item] === undefined) {
            repeticoes[item] = 1
            continue
        }
        repeticoes[item] += 1
    }
    return repeticoes
}