import * as PD from 'probability-distributions'

import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'

export class Uniforme implements GeradorNumerosAleatorios {

    constructor(
        private readonly minimo: number,
        private readonly maximo: number
    ) { }

    gerarProximo(): number {
        return PD.runif(1, this.minimo, this.maximo)
    }
}